const express = require('express');
const router = express.Router();
const controllerProduct = require('../controllers/product');

router.post('/', controllerProduct.createProduct);

router.get('/', controllerProduct.getProducts);

router.get('/:id', controllerProduct.getProductById);

router.put('/:id', controllerProduct.updateProduct);

router.delete('/:id', controllerProduct.deleteProduct);

module.exports = router;