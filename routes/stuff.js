const express = require('express');
const auth = require('../middleware/auth');
const multer = require('../middleware/multer-config');
const router = express.Router();
const controllerStuff = require('../controllers/stuff');

router.post('/', auth, multer, controllerStuff.createThing);

router.get('/', auth, controllerStuff.getThings);

router.get('/:id', auth, controllerStuff.getThingById);

router.put('/:id', auth, multer, controllerStuff.updateThing);

router.delete('/:id', auth, controllerStuff.deleteThing);

module.exports = router;