const {Sequelize, DataTypes} = require('sequelize');

const sequelize = new Sequelize('node_dev', 'postgres', 'chargel', {
    dialect: 'postgres',
    host: 'localhost'
});

sequelize.authenticate()
.then(() => {
    console.log('connected..')
})
.catch(err => {
    console.log('Error'+ err)
})

const db = {}

db.Sequelize = Sequelize
db.sequelize = sequelize

db.products = require('./productModel')(sequelize, DataTypes)

db.sequelize.sync({ force: false })
.then(() => {
    console.log('yes re-sync done!')
});


module.exports = db