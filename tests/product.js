var express = require('express'); // (npm install --save express)
var request = require('supertest');

function createApp() {
  app = express();

  var router = express.Router();
  router.route('api/product').get(function(req, res) {
    return res.json({goodCall: true});
  });

  app.use(router);

  return app;
}

describe('Our server', function() {
  var app;

  // Called once before any of the tests in this block begin.
  before(function(done) {
    app = createApp();
    app.listen(function(err) {
      if (err) { return done(err); }
      done();
    });
  });

  it('should return a 200 status code', async () => {
    request(app).get('/product')
    .expect(200, function(err, res) {
    });
  });

  it('should create a new product', async () => {
    request(app)
      .post('/product')
      .send({ title: 'Test', price: 30, description: 'test product' })
      .expect(201, function(err, res) {
        // Done
      });
  });
});

