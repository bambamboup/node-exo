const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const stuffRouter = require('./routes/stuff')
const productRouter = require('./routes/product')
const userRouter = require('./routes/user')
const path = require('path');


// mongoose.connect('mongodb+srv://nodetest:limahim18@cluster0.s9qastu.mongodb.net/?retryWrites=true&w=majority',
//   { useNewUrlParser: true,
//     useUnifiedTopology: true })
//   .then(() => console.log('Connexion à MongoDB réussie !'))
//   .catch(() => console.log('Connexion à MongoDB échouée !'));

const app = express();

app.use((req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content, Accept, Content-Type, Authorization');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, PATCH, OPTIONS');
    next();
});

app.use(bodyParser.json());

app.use('/api/stuff', stuffRouter);
app.use('/api/product', productRouter);
app.use('/api/auth', userRouter);
app.use('/images', express.static(path.join(__dirname, 'images')))


module.exports = app;