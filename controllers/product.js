const db = require('../models/index');
const Product = db.products

exports.getProducts =  (req, res) => {
    Product.findAll()
            .then(products => res.status(200).json(products))
            .catch(error => res.status(400).json({error}));
}

exports.createProduct = (req, res) => {
    const product = {...req.body};
    Product.create(product)
            .then(() => res.status(201).json(product))
            .catch(error => res.status(400).json({error}))
}

exports.getProductById =  (req, res) => {
    Product.findOne({_id: req.params.id})
            .then(product => res.status(200).json(product))
            .catch(error => res.status(400).json({error}));
}

exports.updateProduct =  (req, res) => {
    Product.update(req.body, {where: {id: req.params.id}})
            .then(() => res.status(200).json({message: 'Modified!'}))
            .catch(error => res.status(400).json({error}));
}

exports.deleteProduct =  (req, res) => {
    Product.destroy({where: {id: req.params.id}})
            .then(() => res.status(200).json({message: 'Deleted!'}))
            .catch(error => res.status(400).json({error}));
}